#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    string line;
    vector <int> ids;
    vector<vector<int>> seats(128, vector<int>(8));
    for (auto& row : seats)
        for (auto& seat : row)
            seat = 0;
    ifstream in("day_five_input.txt");
    int max_id = 0;
    while (getline(in, line)){
        if (line == "\n")
            break;
        int min_row = 0;
        int max_row = 127;
        for (int i = 0; i < 7; i++){
            if (line[i] == 'F')
                max_row -= pow(2, 6 - i);
            else
                min_row += pow(2, 6 - i);
        }
        int min_col = 0;
        int max_col = 7;
        for (int j = 7; j < 10; j++){
            if(line[j] == 'L')
                max_col -= pow(2, 9 - j);
            else
                min_col += pow(2, 9 - j);
        }
        int id = min_row*8+min_col;
        seats[min_row][min_col] = 1;
        ids.push_back(id);
    }
    sort(begin(ids), end(ids));
    /*for (const auto& id : ids)
        cout << id << endl;
    cout << "Start here --------" << endl;
    for (int i = 0; i < 127*8+7; i++){
        if (ids[i] == 69 + i)
            ids[i]++;
        if (ids[i] != 70+i)
            cout << ids[i] << " != " << 70 + i << endl;
    }*/
    for (int i = 0; i < 128; i++){
        for (int j = 0; j < 8; j++)
            if (seats[i][j] == 0)
                cout << "row = " << i << " " << "col = " << j << endl;
    }
}
