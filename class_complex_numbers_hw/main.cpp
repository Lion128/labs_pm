#include <iostream>

using namespace std;

class complex_num{
    private:
        double re;
        double im;
    public:
        complex_num(double re = 0, double im = 0): re(re), im(im){}

        complex_num operator= (complex_num v2){
            re = v2.re;
            im = v2.im;
            return *this;
        }
        friend complex_num operator+ (complex_num, complex_num);
        friend complex_num operator- (complex_num, complex_num);
        friend complex_num operator* (complex_num, complex_num);
        friend complex_num operator/ (complex_num, complex_num);
        friend ostream& operator<< (ostream& out, const complex_num& x);
        int operator!= (complex_num v2){
            if ((re != v2.re) || (im != v2.im))
                return 1;
            else
                return 0;
        }

        double real(){return re;}
        double img(){return im;}
};

complex_num operator+ (complex_num v1, complex_num v2){
    complex_num res;
    res.re = v1.re + v2.re;
    res.im = v1.im + v2.im;
    return res;
}

complex_num operator- (complex_num v1, complex_num v2){
    complex_num res;
    res.re = v1.re - v2.re;
    res.im = v1.im - v2.im;
    return res;
}
complex_num operator* (complex_num v1, complex_num v2){
    complex_num temp;
    temp.re = v1.re*v2.re - v1.im*v2.im;
    temp.im = v1.re*v2.im + v1.im*v2.re;
    return temp;
}
complex_num operator/ (complex_num v1, complex_num v2){
    complex_num tmp;
    int inv = v2.re*v2.re + v2.im*v2.im;
    tmp.re = (v1.re*v2.re + v1.im*v2.im)/inv;
    tmp.im = (-v1.re*v2.im + v1.im*v2.re)/inv;
        return tmp;
}
ostream& operator<< (ostream& out, const complex_num& x){
    return (out << "[" << x.re << ", " << x.im << "]");
}


int test(){
    complex_num first_1(1, 10);
    complex_num second_1(1, -10);

    complex_num third_1 = first_1;
    if(third_1 != first_1)
        printf("= ERROR\n");
    else
        printf("= OK\n");


    third_1 = first_1 + second_1;
    if(third_1 != complex_num(2))
        printf("+ ERROR\n");
    else
        printf("+ OK\n");

    third_1 = first_1 - second_1;
    if(third_1 != complex_num(0, 20))
        printf("- ERROR\n");
    else
        printf("- OK\n");

    third_1 = first_1 * second_1;
    if(third_1 != complex_num(101))
        printf("* ERROR\n");
    else
        printf("* OK\n");

    third_1 = first_1/second_1;
    if((abs(third_1.real() + 0.98) > 0.01) || (abs(third_1.img() - 0.19) > 0.01 ))
        printf("\\ ERROR\n");
    else
        printf("\\ OK\n");
    return 0;
}

int main()
{
    int a, b, c, d;
    cout << "Enter first number" << endl << "re = ";
    cin >> a;
    cout << "Im = ";
    cin >> b;
    cout << "Enter second number" << endl << "re = ";
    cin >> c;
    cout << "Im = ";
    cin >> d;
    complex_num num1(a, b);
    complex_num num2(c, d);
    cout << "num1 + num2 = " << num1 + num2 << endl;
    cout << "num1 - num2 = " << num1 - num2 << endl;
    cout << "num1 * num2 = " << num1 * num2 << endl;
    cout << "num1 / num2 = " << num1 / num2 << endl;
    cout << "default test results" << endl;
    test();
    return 0;
}
