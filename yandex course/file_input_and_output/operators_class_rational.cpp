#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <ctype.h>

using namespace std;

class Rational {
public:
  Rational(){
    p = 0;
    q = 1;
  }
  Rational(int numerator, int denominator){
      if (numerator < 0 && denominator < 0){
        p = -numerator;
        q = - denominator;
      } else if (numerator == 0){
          p = 0;
          q = 1;
      } else if (denominator < 0){
          p = -numerator;
          q = -denominator;
      } else {
          p = numerator;
          q = denominator;
      }
      if (p > 0 && q > 0){
          int devis = gcd(p, q);
          p = p / devis;
          q = q / devis;
      } else {
          //cout << "p = " << p << ", q = " << q << endl;
          int devis = gcd(-p, q);
          p = p / devis;
          q = q / devis;
      }
  }

  int Numerator() const{
      return p;
  }
  int Denominator() const{
      return q;
  }

private:
    int gcd(int first, int second){
        if (first == 0 || second == 0)
            return 1;
        if (first < second){
            int tmp = second;
            second = first;
            first = tmp;
        }
        while (1){
            first = first % second;
            int tmp = first;
            first = second;
            second = tmp;
            if (second == 0){
                return first;
            }
        }
    }
    int p = 0, q = 0;
};

bool operator==(const Rational& first, const Rational& second){
      if (first.Numerator() == second.Numerator() && first.Denominator() == second.Denominator())
            return true;
      return false;
  }
Rational operator+(const Rational& first, const Rational& second){
    int tmp1 = first.Numerator() * second.Denominator();
    int tmp2 = second.Numerator() * first.Denominator();
    int denom = second.Denominator() * first.Denominator();
    return {tmp1+tmp2, denom};
}
Rational operator-(const Rational& first, const Rational& second){
    int tmp1 = first.Numerator() * second.Denominator();
    int tmp2 = second.Numerator() * first.Denominator();
    int denom = second.Denominator() * first.Denominator();
    return {tmp1-tmp2, denom};
}
Rational operator*(const Rational& first, const Rational& second){
    return {first.Numerator() * second.Numerator(), first.Denominator()*second.Denominator()};
}
Rational operator/ (const Rational& first, const Rational& second){
    return {first.Numerator() * second.Denominator(), first.Denominator() * second.Numerator()};
}

ostream& operator<< (ostream& stream, const Rational& rat){
    stream << rat.Numerator();
    stream << "/";
    stream << rat.Denominator();
    return stream;
}

istream& operator>> (istream& stream, Rational& rational){
    int num = -999, denum = -999;
    char separ;
    stream >> num;
    if (!stream.operator bool())
        return stream;
    stream >> separ;
    if (!stream.operator bool())
        return stream;
    stream >> denum;
    if (!stream.operator bool())
        return stream;
    if (separ == '/')
        rational = {num, denum};
    return stream;
}
int main() {
    {
        ostringstream output;
        output << Rational(-6, 8);
        if (output.str() != "-3/4") {
            cout << "Rational(-6, 8) should be written as \"-3/4\"" << endl;
            return 1;
        }
    }

    {
        istringstream input("5/7");
        Rational r;
        input >> r;
        bool equal = r == Rational(5, 7);
        if (!equal) {
            cout << "5/7 is incorrectly read as " << r << endl;
            return 2;
        }
    }

    {
        istringstream input("");
        Rational r;
        bool correct = !(input >> r);
        if (!correct) {
            cout << "Read from empty stream works incorrectly" << endl;
            return 3;
        }
    }

    {
        istringstream input("5/7 10/8");
        Rational r1, r2;
        input >> r1 >> r2;
        bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << endl;
            return 4;
        }


        input >> r1;
        input >> r2;
        correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Read from empty stream shouldn't change arguments 0: " << r1 << " " << r2 << endl;
            return 5;
        }

    }

    {
        istringstream input1("1%2"), input2("1/"), input3("/4");
        Rational r1 = {3, 5};
        Rational r2 = {3, 7};
        Rational r3 = {1, 8};
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational(3, 5) && r2 == Rational(3, 7) && r3 == Rational(1, 8);
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments 1: "
                 <<"r1 = "<< r1 << "; r2 = " << r2 << "; r3 = " << r3 << endl;

            return 6;
        }
    }
    {
        istringstream input1("1%2"), input2("1\\"), input3("/4");
        Rational r1, r2, r3;
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational() && r2 == Rational() && r3 == Rational();
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments 2: "
                 <<"r1 = "<< r1 << "; r2 = " << r2 << "; r3 = " << r3 << endl;

            return 7;
        }
    }
    {
        istringstream input1("1   /   2"), input2("1&"), input3("146");
        Rational r1 = {3, 5};
        Rational r2 = {3, 7};
        Rational r3 = {1, 8};
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational(1, 2) && r2 == Rational(3, 7) && r3 == Rational(1 ,8);
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments 3: "
                 <<"r1 = "<< r1 << "; r2 = " << r2 << "; r3 = " << r3 << endl;

            return 8;
        }
    }
    {
        istringstream input1("1   /   2"), input2("a/2"), input3("146");
        Rational r1;
        Rational r2;
        Rational r3;
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational(1, 2) && r2 == Rational(0, 1) && r3 == Rational(0 ,1);
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments: "
                 <<"r1 = "<< r1 << "; r2 = " << r2 << "; r3 = " << r3 << endl;

            return 9;
        }
    }

    cout << "OK" << endl;
    return 0;
}
