#include <iostream>
#include <cmath>

using namespace std;

int main(){
    int p = 79, q = 67;
    int n = p*q;
    int r = (p-1)*(q-1);// = 5148
    int e = 4909; // Has to be prime number, has to be < r, has to be prime with r - so cold - open exponent
    /*
    NOW pubic key:
    e = 5
    n = 21

    secret key d, n : (d*e)%r    = 1
    */
    int d;
    for (d = 1; (d * e)%r != 1; d++);
    cout << d;



    // Sending
    int information_my = 4740;
    long long int email = 1;
    for (int i = 0; i < e; i++){
        email = (email*information_my)%n;
        //cout << "i = " << i << " email = " << email << endl;
    }
    email = email % n;
    cout << email << endl;

    long long int my_info = 1;
    for (int i = 0; i < d; i++){
        my_info = (my_info*email)%n;
    }
    cout << "my_info = " << my_info << endl;


    return 0;
}
