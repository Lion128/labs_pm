#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <exception>

using namespace std;

class Date{
public:
    Date(){}
    Date(int new_year, int new_month, int new_day){
        this->year = new_year;
        this->month = new_month;
        this->day = new_day;

    }
    int GetYear() const{
        return year;
    }
    int GetMonth() const{
        return month;
    }
    int GetDay() const{
        return day;
    }
private:
    int year;
    int month;
    int day;
};

bool operator< (const Date& lhs, const Date& rhs){
    if (lhs.GetYear() != rhs.GetYear()){
        return lhs.GetYear() < rhs.GetYear();
    } else {
        if (lhs.GetMonth() != rhs.GetMonth()){
            return lhs.GetMonth() < rhs.GetMonth();
        } else {
            return lhs.GetDay() < rhs.GetDay();
        }
    }
}
istream& operator>> (istream& is, Date& dt){
    string error_ms;
    int year = 0, month = 0, day = 0;
    char dif1, dif2;
    is >> year;
    is >> dif1;
    is >> month;
    is >> dif2;
    is >> day;
    if (!is.operator bool()){
        error_ms = to_string(year) + dif1 + to_string(month) + dif2 + to_string(day);
        throw runtime_error(error_ms);
        return is;
    }
    if (dif1 == '-' && dif2 == '-')
        dt = {year, month, day};
    else{
        error_ms = to_string(year) + dif1 + to_string(month) + dif2 + to_string(day);
        throw runtime_error(error_ms);
        return is;
    }
    return is;
}
class Database{
public:
    void AddEvent(const Date& date, const string& event){
        (this->base[date]).insert(event);
        return;
    };
    bool DeleteEvent(const Date& date, const string& event){
        if ((this->base.count(date))> 0 && this->base[date].count(event) > 0){
            this->base[date].erase(event);
            return true;
        } else {
            return false;
        }
    };
    int DeleteDate(const Date& date){
        if (this->base.count(date) > 0){
            int number_of_events = this->base[date].size();
            this->base.erase(date);
            return number_of_events;
        } else{
            return 0;
        }
    };
    void Find(const Date& date) const{
        if (this->base.count(date) > 0){
            int size_of_set = this->base.at(date).size();
            for (const auto& element_from_set : this->base.at(date)){
                cout << element_from_set << endl;
            }
        } else {
            return;
        }
    };
    void Print() const{
        for (const auto& [key, value] : this->base){
            for (const auto& set_element : value){
                cout << setw(4) << setfill('0') << key.GetYear() << "-"
            << setw(2) << setfill('0') << key.GetMonth() << "-"
            << setw(2) << setfill('0') << key.GetDay() << " ";
                cout << set_element << endl;
            }
        }
    };
private:
    map<Date, set<string>> base;
};

int main(){
    Database db;
    string command;
    while(getline(cin, command)){
        stringstream s(command);
        string cmd;
        s >> cmd;
        if (cmd == "Add"){
            Date dt;
            try{
                s >> dt;
                if (dt.GetMonth() < 1 || dt.GetMonth() > 12){
                    cout << "Month value is invalid: " << dt.GetMonth() << endl;
                    break;
                }
                if (dt.GetDay() < 1 || dt.GetDay() > 31){
                    cout << "Day value is invalid: " << dt.GetDay() << endl;
                    break;
                }
                string event;
                s >> event;
                db.AddEvent(dt, event);
            } catch (exception &e){
                string error_ms;
                for (int i = 4; command[i] != ' '; i++)
                    error_ms += command[i];
                cout << "Wrong date format: " << error_ms << endl;
            }

        } else if (cmd == "Print"){
            db.Print();
        } else if (cmd == "Del"){
            int year, month, day;
            s >> year;
            s.ignore(1);
            s >> month;
            s.ignore(1);
            s >> day;
            string event;
            s >> event;
            Date dt = {year, month, day};
            if (!event.empty()){
                if (db.DeleteEvent(dt, event))
                    cout << "Deleted successfully" << endl;
                else
                    cout << "Event not found" << endl;
            } else {
                cout <<"Deleted " <<  db.DeleteDate(dt) << " events" << endl;
            }

        } else if (cmd == "Find") {
            int year, month, day;
            Date dt;
            s >> dt;
            db.Find(dt);
        } else if (cmd == "") {
            ;
        } else {
            cout << "Unknown command: " << cmd << endl;
        }
    }
    return 0;
}
