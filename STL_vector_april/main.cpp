#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <fstream>
using namespace std;


int main()
{
    // Setting the lacalization
    setlocale(LC_ALL, "RUS");
    // Opening the file
    FILE* fin;
    cout << "Input the name of the input file(with data): ";
    char s[256];
    cin.getline(s, 256, '\n');
    fin = fopen(s, "r");
    // Check if the file was opened correctly
    if(!fin){
        perror("���������� ������� ���� ");
        return -1;
    }
    int n = 0;
    int ch;
    // Looking through the file "int by int", counting the number of elements in the file
    while (fscanf(fin, "%d", &ch) != -1){
            n++;
    }
    //cout << n << endl;
    // There are 2 elements if each line, we will read line by line, so we divide by 2
    n = n / 2;
    // Redefining the pair type as SV to use in vector template class
    typedef pair<int, int> SV;
    vector<SV> v(n);
    // Moving the pointer *fin to the start of the file
    rewind(fin);
    // Reading each line 2 symbols at once
    for (int i = 0; i < n; i++)
        fscanf(fin, "%d %d", &v[i].first, &v[i].second);
    // Closing the file
    fclose(fin);


    // The exercise program starts here - regression analysis
    int sum1 = 0, sum2 = 0;
    for (int i = 0; i < n; i++){
        sum1+= v[i].first;
        sum2+= v[i].second;
    }
    double avg_x = (double)sum1/n; // M(x)
    double avg_y = (double)sum2/n; // M(y)
    double sum_d_x = 0, sum_d_y = 0;
    for(int i = 0; i < n; i++){
        sum_d_x+= (v[i].first - avg_x) * (v[i].first - avg_x);
        sum_d_y+= (v[i].second - avg_y) * (v[i].second - avg_y);
    }
    double d_x = sum_d_x / n; // D(x)
    double d_y = sum_d_y / n; // D(y)
    double s_x = sqrt(d_x); // S(x)
    double s_y = sqrt(d_y); // S(y)


    double r = 0;
    for(int i = 0; i < n; i++){
        r+= (v[i].first - avg_x) * (v[i].second - avg_y);
    }
    if( (n * s_x * s_y) == 0){
        perror("Divison by zero");
        return -2;
    }
    r =r / (n * s_x * s_y);

    vector<double> yr_vector(n);
    for (int i = 0; i < n; i++){
        double yr = avg_y + s_y/s_x * r * (v[i].first - avg_x);
        yr_vector[i] = yr;
    }


    FILE* fout;
    fout = fopen("test.txt", "w+");
    if(!fout){
        cout << "���� ������� ���������� test.txt";
        return -1;
    }
    fprintf(fout, "M(x) = %lf\nM(y) = %lf\nD(x) = %lf\nD(y) = %lf\nS(x) = %lf\nS(y) = %lf\nR(x, y) = %lf\n", avg_x, avg_y, d_x, d_y, s_x, s_y, r);
    fprintf(fout, "%s %s  %s\n", "Weight", "height", "Computed height");
    for(int i = 0; i < n; i++){
        fprintf(fout, "%-7d %-7d  %-10lf\n", v[i].first, v[i].second, yr_vector[i]);
    }
    fclose(fout);
    cout << "OK: " << "��������� ���� test.txt" << endl;
    return 0;

}
