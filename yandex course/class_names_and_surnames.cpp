#include <iostream>
#include <string>
#include <map>
using namespace std;

class Person{
public:
    void ChangeFirstName(int year, const string& first_name){
        name_history[year] = first_name;
    }
    void ChangeLastName(int year, const string& last_name){
        surname_history[year] = last_name;
    }
    string GetFullName(int year){
        string first_name = GetName(year);
        string last_name = GetSurname(year);
        if (first_name.empty() && last_name.empty()){
            return "Incognito";
        }
        else if (first_name.empty()){
            return last_name + " with unknown first name";
        }
        else if (last_name.empty()){
            return first_name + " with unknown last name";
        }
        else {
            return first_name + " " + last_name;
        }
    }
private:
    map<int, string> name_history;
    map<int, string> surname_history;
    string GetName(int year){
        string name;
        int name_year = 0;
        for (const auto& item : name_history)
            if (item.first <= year)
                name_year = item.first;
        if (name_year != 0)
            name = name_history[name_year];
        return name;
    }
    string GetSurname(int year){
        int surname_year = 0;
        string surname;
        for (const auto& item : surname_history)
            if (item.first <= year)
                surname_year = item.first;
        if (surname_year != 0)
            surname = surname_history[surname_year];
        return surname;
    }
};
int main () {

    Person person;

    int year = 1;
    person.ChangeFirstName(year, std::to_string(year)+"_first");
    person.ChangeLastName(year, std::to_string(year)+"_last");
    std::cout << "year: " << year << '\n';
    std::cout << person.GetFullName(year) << '\n';

    year = 2;
    person.ChangeFirstName(year, std::to_string(year)+"_first");
    person.ChangeLastName(year, std::to_string(year)+"_last");
    std::cout << "year: " << year << '\n';
    std::cout << person.GetFullName(year) << '\n';

    year = 3;
    person.ChangeFirstName(year, std::to_string(2)+"_first");
    person.ChangeLastName(year, std::to_string(2)+"_last");
    std::cout << "year: " << year << '\n';
    std::cout << person.GetFullName(year) << '\n';


return 0;
}
