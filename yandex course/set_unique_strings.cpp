#include <iostream>
#include <string>
#include <set>

using namespace std;

int main(){
    set<string> s;
    int n;
    cin >> n;
    string insert_string;
    for (int i = 0; i < n; ++i){
        cin >> insert_string;
        s.insert(insert_string);
    }
    cout << s.size();
    return 0;
}
