#include <iostream>
#include <string>
#include <map>
#include <set>

using namespace std;

int main(){
    map<set<string>, int> buses;
    int number_of_operations;
    cin >> number_of_operations;
    int counter = 0;
    for (int i = 0; i < number_of_operations; i++){
        int k = 0;
        cin >> k;
        set<string> stops;
        while (k > 0){
            k--;
            string stop;
            cin >> stop;
            stops.insert(stop);
        }
        if (buses[stops]){
            cout << "Already exists for " << buses[stops] << endl;
        }else{
            counter++;
            buses[stops] = counter;
            cout << "New bus " << counter << endl;
        }
    }
    return 0;
}
