#include <iostream>
#include <vector>
#include <utility>
#include <map>
#include <string>

using namespace std;

template <typename First, typename Second>
pair<First, Second> operator*(pair<First, Second>& p1, pair<First, Second>& p2){
    cout << " operator * in operation" << endl;
    cout << p1.first * p1.first << ", " << p1.second * p1.second << endl;
    p1.first = p1.first * p1.first;
    p1.second = p1.second * p1.second;
    return p1;
}

template<typename First, typename Second>
map<First, Second> operator*(const map<First, Second>& m1, const map<First, Second>& m2){
    map<First, Second> m01 = m1;
    map<First, Second> m02 = m2;
    for(auto& [key, value] : m01){
        value = value * value;
    }
    return m1;
}

template<typename T>
T Sqr(const T& v){
    T new_v = v;
    for (auto& c : new_v)
        c = c*c;
    return new_v;
}

template <typename First, typename Second>
pair<First, Second> Sqr(const pair<First, Second>& p){
    pair<First, Second> res = p * p;
    return res;
}

template<typename First, typename Second>
map<First, Second> Sqr(const map<First, Second>& m){
    map<First, Second> res = m * m;
    return res;
}
int main(){
    vector<int> v = {1, 2, 3};
cout << "vector:";
for (int x : Sqr(v)) {
  cout << ' ' << x;
}
cout << endl;

map<int, pair<int, int>> map_of_pairs = {
  {4, {2, 2}},
  {7, {4, 3}}
};
cout << "map of pairs:" << endl;
for (const auto& x : Sqr(map_of_pairs)) {
  cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
}
    return 0;
}
