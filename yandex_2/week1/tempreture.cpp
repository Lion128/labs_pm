#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<int> t(n);
    vector<int> out;
    int64_t sum = 0;
    for (int& c : t){
        cin >> c;
        sum += c;
    }
    int avg = sum / static_cast<int>(t.size());
    for (int i = 0; i < static_cast<int>(t.size()); i++)
        if (t[i] > avg){
            out.push_back(i);
        }
    cout << static_cast<int>(out.size()) << endl;
    for (const int& c : out)
        cout << c << " ";
    return 0;
}
