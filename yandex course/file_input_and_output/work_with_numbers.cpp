#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

int main(){
    ifstream input("input.txt");
    if (input){
        int n, m;
        input >> n >> m;
        int number = 0;
        cout.fill('*');
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                input >> number;
                input.ignore(1);
                cout << setw(10) << number;
                if (j+1 < m)
                    cout << " ";
            }
            cout << endl;
        }
    }
    return 0;
}
