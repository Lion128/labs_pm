#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

int main(){
    map <vector<string>, int> buses;
    int n;
    cin >> n;
    for (int i = 0; i < n; i++){
        int k;
        cin >> k;
        vector<string> stops;
        for (int j = 0; j < k; j++){
            string stop;
            cin >> stop;
            stops.push_back(stop);
        }
        if (buses[stops]){
            cout << "Already exists for " << buses[stops] << endl;
        } else {
            int number = buses.size();
            buses[stops] = number;
            cout << "New bus " << number << endl;
        }
    }
    return 0;
}
