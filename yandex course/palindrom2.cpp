#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector <string> PalindromFilter(vector <string> words, int min_len){
    vector <string> res;
    int flag = 1;
    for (auto word : words){
            flag = 1;
        if (word.size() >= min_len){
            for (int i = 0; i < word.size()/2; ++i){
                if (word[i] != word[word.size() - 1 - i]){
                    flag = 0;
                    break;
                }
            }
            if (flag)
                res.push_back(word);
        }

    }
    return res;
}

int main(){
    vector <string> ans;
    ans = PalindromFilter({"abba", "milk", "shake", "air", "abcd", "asd", "aa", "bro", "weew", "abba", "code", "abcba", "a", "cdeffedcba"}, 4);
    for (auto c : ans)
        cout << c << " ";
    return 0;
}
