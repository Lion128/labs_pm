#include <iostream>
#include <cmath>
using namespace std;


int finddet(int**, int);
template<typename Type>
class matrix;
template<class T>
ostream& operator<< (ostream &out, matrix<T>& x);

// ��������� ��������� +  ��� ���� ����� �.�. � ���� �������
template <class T>
matrix<T> operator+ (matrix<T> a, matrix<T> b){
    T** p;
    p = new T* [a.n];
    for(int k = 0; k < a.n; k++)
            *(p+k) = new T[a.m];
    for(int i = 0; i < a.n; i++)
        for(int j = 0; j < a.m; j++){
            *(*(p + i) + j) = *(*(a.get() + i) + j) + *(*(b.get() + i) + j);
        }
    matrix<T> res(p, a.n, a.m);

    for (int i = 0; i < a.n; i++)
        delete [] *(p+i);
    delete[] p;

    return res;
}

template <class T>
matrix<T> operator- (matrix<T> a, matrix<T> b){
    T** p;
    p = new T* [a.n];
    for(int k = 0; k < a.n; k++)
            *(p+k) = new T[a.m];
    for(int i = 0; i < a.n; i++)
        for(int j = 0; j < a.m; j++){
            *(*(p + i) + j) = *(*(a.get() + i) + j) - *(*(b.get() + i) + j);
        }
    matrix<T> res(p, a.n, a.m);

    for (int i = 0; i < a.n; i++)
        delete [] *(p+i);
    delete[] p;

    return res;
}

template <class T>
matrix<T> operator* (matrix<T> a, matrix<T> b){
    int q = a.n, g = b.n, v = b.m;
    T** p;
    p = new T* [q];
    for(int k = 0; k < q; k++)
            *(p+k) = new T[v];
    for(int i = 0; i < q; i++)
        for(int j = 0; j < v; j++){
            for(int k = 0; k < g; k++)
            *(*(p + i) + j) += *(*(a.get() + i) + k) * *(*(b.get() + k) + j);
        }
    matrix<T> res(p, q, v);

    for (int i = 0; i < q; i++)
        delete [] *(p+i);
    delete[] p;

    return res;
}



template <class X>
class matrix{
public:
    int n;
    int m;
    X **t;

    matrix(){
        n = 0;
        m = 0;
    }
    matrix(int row_number, int col_number) // ����������� ������� n * m - ������������ ��������� ������
    {
        n = row_number;
        m = col_number;
        t = new X* [n];
        for(int k = 0; k < n; k++)
            *(t+k) = new X[m];
        for(int i = 0; i < n; i++){
            for(int  j = 0; j < m; j++){
                cout << "Inter the element" << " [" << i << "] [" << j << "] :" << " ";
                cin >> t[i][j];
            }
        }
    }
    matrix(X** mas, int q, int w){

        this -> n = q;
        this -> m = w;
        //cout << "n = " << n << endl << "m = " << m << endl;
        /*cout << "CHECK IN CONSTRUCTOR" << endl;
        for (int i = 0; i < n; ++i)
            for(int j = 0; j < m; ++j)
                cout << *(*(mas + i) + j) << endl;
*/

        t = new X* [n];
        for(int k = 0; k < n; k++)
            *(t+k) = new X[m];


        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j){
                //cout << "COUNT THIS" << endl;
                //cout << "i = " << i << "; j = " << j <<endl;
                *(*(t + i) + j) = *(*(mas + i) + j);
            }
    }
    ~matrix(){
        for (int i = 0; i < n; i++)
            delete [] *(t+i);
        delete[] t;
    }
    int** get(){
        return t;
    }
    template<typename T>
    friend matrix operator+ (matrix<X> a, matrix<X> b);
    template<typename T>
    friend matrix operator- (matrix<X> a, matrix<X> b);
    template<typename T>
    friend matrix operator* (matrix<X> a, matrix<X> b);
    template<typename T>
    friend ostream& operator<< (ostream& out,  const matrix<T>& x);

    void show(){
        //cout << "start SHOW" << endl;
        for(int i = 0; i < this->n; i++){
            for(int  j = 0; j < this->m; j++){
                //cout << "i = " << i << "; j = " << j <<endl;
                cout << *(*(t + i) + j) << " ";
            }
            cout << endl;
        }
        //cout << "end SHOW" << endl;
        return;
    }

    int det(){
        if (m == n)
            return finddet(t, n);
        else
            cout << "Det Error" << endl;
        return 0;
    }
    int gdet(){
        int tmp;
        for (int k = 0; k < n - 1; k++) {
            for (int i = k + 1; i < n; i++) {
                if (t[k][k] != 0){
                    tmp = -t[i][k] / t[k][k];
                }
                else
                    continue;
                for (int j = 0; j < n; j++) {
                    t[i][j] += t[k][j] * tmp;
                }
            }
        }
    //counting det by main diag
    int d = 1;
    for (int i = 0; i < n; i++) {
        d *= t[i][i];
        }
    return d;
    }


    int inv(){
        int d = finddet(t, n);
        if (!d){
            cout << "No inv matrix" << endl;
            return 0;
        }
        X** rev;
        rev = new X* [n];
        for(int k = 0; k < n; ++k)
            *(rev + k) = new X[m];

        for(int s = 0; s < n; ++s){
            for(int c = 0; c < m; ++c){

                X** tmp;
                tmp = new X* [n-1];
                for(int h = 0; h < n-1; ++h)
                    *(tmp + h) = new X[m-1];

                int p = 0;
                for(int i = 0; i < n; ++i){
                    int q = 0;
                    if (s != i){
                        for(int j = 0; j < m; ++j){
                            if (c != j){
                               tmp[p][q] = t[i][j];
                               q++;
                            }
                        }
                        p++;
                    }
                }
                rev[s][c] = finddet(tmp, n-1);
            }
        }

        // �������� rev - ������� �������
        for(int i = 0; i < n; ++i){ // ������� �������������� ����������
            for(int j = 0; j < m; ++j){
                if ((i+j)%2)
                    rev[i][j] = -rev[i][j];
            }
        }
        // ������������� ������� ��� ���������� rev
        X tmp;
        for (int i = 0; i < n; ++i){
            for(int j = 0; j < i; ++j){
                tmp = rev[i][j];
                rev[i][j] = rev[j][i];
                rev[j][i] = tmp;
            }
        }

        double** res;
        res = new double* [n];
        for(int k = 0; k < n; ++k)
            *(res + k) = new double[m];
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < m; ++j)
                res[i][j] = rev[i][j]/(double)d;

        cout << "INV MATRIX" << endl;
        for(int i = 0; i < n; ++i){
            for(int j = 0; j < m; ++j){
                printf("%7.1lf" ,res[i][j]);
            }
            cout << endl;
        }
        return 0;
    }



    int transpone(){
        X tmp;
        for (int i = 0; i < n; ++i){
            for(int j = 0; j < i; ++j){
                tmp = t[i][j];
                t[i][j] = t[j][i];
                t[j][i] = tmp;
            }
        }
        return 0;
    }
};

template <class T>
ostream & operator<< (ostream &out, matrix<T>& x)
{
    for(int i = 0; i < x.n; i++){
        for(int  j = 0; j < x.m; j++)
            out << *(*(x.t + i) + j) << " ";
        out << endl;
    }
    return out;
}
void clearMemory(int** a, int n) { //������� ������������ ������, ���������� ��� ��������� ������������ ������
    for (int i = 0; i < n; i++) {
            delete[] a[i];
        }
        delete [] a;
}



int finddet(int** a, int n) { //����������� ������� ���������� ������������ �������
    if (n == 1)
        return a[0][0];
        else if (n == 2)
            return a[0][0] * a[1][1] - a[0][1] * a[1][0];
        else {
            int d = 0;
            for (int k = 0; k < n; k++) {
                int** m = new int*[n-1];
                    for (int i = 0; i < n - 1; i++) {
                        m[i] = new int[n - 1];
                }
                for (int i = 1; i < n; i++) {
                    int t = 0;
                    for (int j = 0; j < n; j++) {
                        if (j == k)
                            continue;
                        m[i-1][t] = a[i][j];
                        t++;
                    }
                }
                d += pow(-1, k + 2) * a[0][k] * finddet(m, n - 1);
                clearMemory(m, n - 1); //����������� ������, ���������� ��� �������������� ����������
            }
            return d; //���������� ������������ �������
        }
    }


int main()
{
    int row, col;
    setlocale(LC_ALL, "Rus");
    cout << "Enter the size n*n of the matrix" << endl;

    cout << "number of rows: ";
    cin >> row;
    cout << "number of columns: ";
    cin >> col;
    matrix<int> ob1(row, col);

    //ob.show();
    //cout << "Det = " << ob.det() << endl;;
    //ob.show();
    //cout << "Gauss det = " << ob.gdet() << endl;
    //ob.show();
    //ob.transpone():
    //ob.inv();
    cout << "FIRST" << endl;
    ob1.show();
    cout << "number of rows: ";
    cin >> row;
    cout << "number of columns: ";
    cin >> col;
    matrix<int> ob2(row, col);
    cout << "SECOND" << endl;
    ob2.show();

    matrix<int> sum = ob1 + ob2;
    //cout << "SUM.t = " << **sum.t << endl;
    cout << "Sum:" << endl;
    sum.show();
    matrix<int> dif = ob1 -  ob2;
    //cout << "SUM.t = " << **sum.t << endl;
    cout << "dif:" << endl;
    dif.show();
    /*int n = 2, m = 3;
    int** t;
    t = new int* [n];
        for(int k = 0; k < n; k++)
            *(t+k) = new int[m];
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            *(*(t+i) + j) = i * j + 1;
    matrix<int> obj(t, n, m);
    obj.show();
*/
    cout << "FIRST" << endl;
    ob1.show();
    cout << "SECOND" << endl;
    ob2.show();
    cout << "OVERLOADED OREPATOR <<" << endl;
    cout << ob2 << endl;


    matrix<int> multi = ob1 * ob2;
    cout << "MULTI:" << endl;
    cout << multi;
/*
    matrix<int> dif = ob1 - ob2;
    cout << "dif:" << endl;
    dif.show();
    cout << "FIRST" << endl;
    ob1.show();
    cout << "SECOND" << endl;
    ob2.show();
*/
    return 0;
}
