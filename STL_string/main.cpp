#include <iostream>
#include <string>
#include <vector>
using namespace std;

string devisioner(string, string);
string visioner(string, string);
int main()
{
    // Visioner

    setlocale(LC_ALL, "");
    string str_1;
    cout << "������� ������, ������� ����� �����������: ";
    getline(cin, str_1);

    string str_2_2;
    cout << "������� ������, ������� �� ������ ���������: ";
    getline(cin, str_2_2);

    string shifr = visioner(str_1, str_2_2);
    string start = devisioner(shifr, str_2_2);

    cout << "After shifring: ";
    for (auto c : shifr)
        cout << c;
    cout << endl << "After deshifring: ";
    for (auto c : start)
        cout << c;
    return 0;
}
string visioner(string str_1, string str_2_2){
    string str_2;
    int times = str_1.length() / str_2_2.length() + 1;
    for (int i = 0; i < times; i++)
        str_2 += str_2_2;

    // a - 97, z  - 122; A - 65, Z - 90;
    // ������������ ��� ���� A = 0, Z = 25, a = 26, z = 51
    vector <int> vec_new_1(str_1.length());
    for (unsigned int i = 0; i < str_1.length(); i++){
        if (str_1[i] >= (int) 'a' && str_1[i] <= (int) 'z')
            vec_new_1[i] = (int) str_1[i] - (int) 'a' + 26;
        else if (str_1[i] >= (int) 'A' && str_1[i] <= (int) 'Z')
            vec_new_1[i] = (int) str_1[i] - (int) 'A';
        else if (str_1[i] == ' ')
            vec_new_1[i] = 52;
    }

    vector <int> vec_new_2(str_2.length());
    for (unsigned int i = 0; i < str_2.length(); i++){
        if (str_2[i] >= (int) 'a' && str_2[i] <= (int) 'z')
            vec_new_2[i] = (int) str_2[i] - (int) 'a' + 26;
        else if (str_2[i] >= (int) 'A' && str_2[i] <= (int) 'Z')
            vec_new_2[i] = (int) str_2[i] - (int) 'A';
        else if (str_2[i] == ' ')
            vec_new_2[i] = 52;
    }
    int total = 53; // number of letters in the alphabet
    for (unsigned int i = 0; i < vec_new_1.size(); i++){
        vec_new_1[i] += vec_new_2[i];
        vec_new_1[i] %= total;
    }

    string extra;
    string total_string;
    for (unsigned int i = 0; i < vec_new_1.size(); i++){
        int c = vec_new_1[i];
        if(c <= 25)
            extra = (char)(c + (int)'A');
        else if (c <= 51)
            extra = (char)(c + (int)'a' - 26);
        else if (c == 52)
            extra = ' ';
        total_string.append(extra);
    }
    return total_string;

}
string devisioner(string str_2, string keystring){
    string str;
    int times = str_2.length() / keystring.length() + 1;
    for (int i = 0; i < times; i++)
        str += keystring;

    vector <int> vec_new_1(str_2.length());
    for (unsigned int i = 0; i < str_2.length(); i++){
        if (str_2[i] >= (int) 'a' && str_2[i] <= (int) 'z')
            vec_new_1[i] = (int) str_2[i] - (int) 'a' + 26;
        else if (str_2[i] >= (int) 'A' && str_2[i] <= (int) 'Z')
            vec_new_1[i] = (int) str_2[i] - (int) 'A';
        else if (str_2[i] == ' ')
            vec_new_1[i] = 52;
    }

    vector <int> vec_new_2(str.length());
    for (unsigned int i = 0; i < str.length(); i++){
        if (str[i] >= (int) 'a' && str[i] <= (int) 'z')
            vec_new_2[i] = (int) str[i] - (int) 'a' + 26;
        else if (str[i] >= (int) 'A' && str[i] <= (int) 'Z')
            vec_new_2[i] = (int) str[i] - (int) 'A';
        else if (str[i] == ' ')
            vec_new_2[i] = 52;
    }
    int total = 53;
    vector<int> devis(str_2.length());
    for (unsigned int i = 0; i < str_2.length(); i++){
        devis[i] = (vec_new_1[i] - vec_new_2[i] + total) % total;
    }

    string extra;
    string total_string;
    for (unsigned int i = 0; i < devis.size(); i++){
        int c = devis[i];
        if(c <= 25)
            extra = (char)(c + (int)'A');
        else if (c <= 51)
            extra = (char)(c + (int)'a' - 26);
        else if (c == 52)
            extra = ' ';
        total_string.append(extra);
    }
    return total_string;
}
