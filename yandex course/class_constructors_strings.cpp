#include <iostream>
#include <string>

using namespace std;

class ReversibleString{
public:
    ReversibleString(){}
    ReversibleString(string a){
        str = a;
    }
    void Reverse(){
        string str_new;
        int n = str.size();
        for (int i = n - 1; i >= 0; i--)
            str_new += str[i];
        str = str_new;
    }
    string ToString() const {
        return str;
    }
private:
    string str;

};
int main(){

    ReversibleString s("live");
    s.Reverse();
    cout << s.ToString() << endl;
    s.Reverse();
  const ReversibleString& s_ref = s;
  string tmp = s_ref.ToString();
  cout << tmp << endl;

  ReversibleString empty;
  cout << '"' << empty.ToString() << '"' << endl;

    return 0;
}
