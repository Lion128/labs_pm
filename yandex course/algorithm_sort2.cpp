#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<string> v(n);
    for (string& i : v){
        cin >> i;
    }
    sort(begin(v), end(v),
          [](string s1, string s2){
         for (int i = 0; i < s1.size() && i < s2.size(); i++){
             char c1, c2;
             c1 = tolower(s1[i]);
             c2 = tolower(s2[i]);
             if (c1 < c2)
                return true;
             if (c1 > c2)
                return false;
         }
         });
    for (const string& i : v){
        cout << i << " ";
    }
    return 0;
}
