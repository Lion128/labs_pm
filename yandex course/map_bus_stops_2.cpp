#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

void print_for_map(const map<int, vector<string>>& x){
    for (int i = 0; i < x.size(); i++){
        vector<string> stops = x.at(i);
        cout << i << " ";
        for(auto c : stops){
            cout << c << " ";
        }
    cout << endl;
    }
}

int check_for_identity(const vector<string>& stops, const map<int, vector<string>>& buses){
    int flag = 0;
    for (int i = 0; i < buses.size(); i++){
        const vector<string>& x = buses.at(i);
        //cout << stops.size() << " : " << x.size() << endl;
        for (int j = 0; j < stops.size() && stops.size() == x.size(); j++){
            if (stops.at(j) == x.at(j))
                flag++;
        }
        //cout << "FLAG = " << flag << endl;
        if (flag == stops.size()){
            flag = 0;
            return i;
        }
        //cout << i << ": " << flag << endl;
    }
    return -1;
}


int main(){

    map<int, vector<string>> buses;
    int counter = 0;

    int q;
    cin >> q;
    int number_of_stops;
    string name_of_stop;
    vector<string> stops;
    for (int i = 0; i < q; i++){
        cin >> number_of_stops;
        for (int j = 0; j < number_of_stops; j++){
            cin >> name_of_stop;
            stops.push_back(name_of_stop);
        }
        int ans = check_for_identity(stops, buses);
        //cout << "ANS = " << ans << endl;
        if (ans == -1){
            buses[counter] = stops;
            cout << "New bus " << counter + 1<< endl;
            counter++;
        }
        else{
            cout << "Already exists for " << ans  + 1<< endl;
        }
        stops.clear();
    }
    //print_for_map(buses);
    return 0;
}
