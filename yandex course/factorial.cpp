#include <iostream>

using namespace std;

int Factorial(int x){
    int sum = 1;
    for (int i = 1; i <= x; ++i){
        sum *= i;
    }
    return sum;
}
int main(){
    int a;
    cin >> a;
    cout << Factorial(a) << endl;
    return 0;
}
