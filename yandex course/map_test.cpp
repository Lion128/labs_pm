#include <iostream>
#include <map>
#include <string>
#include <vector>
using namespace std;

map <char, int> BuildCharCounters(const string& x){
    map <char, int> result;
    for (int i = 0; i < x.size(); ++i){
        ++result[x[i]];
    }
    return result;
}

void PrintMap(const map<char, int>& a){
    for(const auto& [key, value] : a){
        cout << key << ":" << value << endl;
    }
}
/*
bool CompareMap(map<char, int>& a, map<char, int>& b){
    for(const auto& item : a){
        if (item.second != b[item.first])
            return false;
    }
    for(const auto& item : b){
        if (a[item.first] != item.second)
            return false;
    }
    return true;
}
*/
int main(){
    int n;
    cin >> n;
    string a;
    string b;
    map <char, int> map_a;
    map <char, int> map_b;
    vector<int> res;
    for(int i = 0; i < n; i++){
        cin >> a;
        cin >> b;
        /*map_a = BuildCharCounters(a);
        map_b = BuildCharCounters(b);
        res.push_back(CompareMap(map_a, map_b));
        */
        if (BuildCharCounters(a) == BuildCharCounters(b))
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    /*
    for (const auto& item : res){
        if(item)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
    */
    return 0;
}
