#include <iostream>
#include <cstdlib>

using namespace std;

typedef struct Vertex{
        int data;
        struct Vertex* left;
        struct Vertex* right;
    } Vertex;

void tree_print(Vertex* tree){  //DFS
    if(tree == NULL)
        return;
    tree_print(tree->left);
    cout << tree->data << " ";
    tree_print(tree->right);
    return;
}
void print(Vertex* tree){
    tree_print(tree);
    cout << endl;
    return;
}

Vertex* tree_add(Vertex* tree, int number){
    if (tree == NULL){
        Vertex * tree_new =(Vertex*) malloc(sizeof(Vertex));
        tree_new -> data = number;
        tree_new -> left = tree_new -> right = NULL;
        return tree_new;
    }
    if (number < tree -> data)
        tree -> left = tree_add(tree -> left, number);
    if (number > tree -> data)
        tree -> right = tree_add(tree->right, number);
    return tree;
}

Vertex* tree_search(Vertex* tree, int number){
    if (tree == NULL){
            cout << "Not found: " << number << endl;
        return NULL;
    }
    if (number > tree -> data){
        tree -> right = tree_search(tree -> right, number);
    }
    if (number < tree -> data){
        tree -> left = tree_search(tree -> left, number);
    }
    if(number == tree -> data){
            cout << "Found: " << tree -> data << endl;;
    }
    return tree;
}

int main()
{
    setlocale(LC_ALL, "");
    Vertex* tree = NULL;
    //Vertex one = {1, NULL, NULL};
    /* Vertex two = {2, NULL, NULL};
    Vertex three = {3, NULL, NULL};
    Vertex four = {4, NULL, NULL};
    Vertex five = {5, NULL, NULL};
    tree = &three;
    three.left = &two;
    two.left = &one;
    three.right = &four;
    four.right = &five;
    */
    //print(tree);
    //tree = tree_add(tree, 1);
    //print(tree);

    // ����������� ���������
    int n = 300;
    int* mas = (int*) malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++){
        mas[i] = rand()%1000 - 500;
        cout << mas[i] << " ";
        tree = tree_add(tree, mas[i]);
    }
    free(mas);
    cout << endl;
    //tree = tree_add(tree, 239);
    print(tree);
    Vertex* t= tree_search(tree, 239);
    return 0;
}
