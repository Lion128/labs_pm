#include <iostream>

using namespace std;

int main(){
    int a, b, deg;
    cin >> a >> b;
    while (a != 0 && b != 0){
        if (a > b){
            a %= b;
        }
        else if (b > a){
            b %= a;
        }
        if (a == b){
            break;
        }
    }
    if (a > b)
        cout << a;
    else
        cout << b;

    return 0;
}
