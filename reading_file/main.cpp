#include <iostream>

using namespace std;

int main()
{
    FILE *in;
    in = fopen("test.txt", "r");
    int num;
    int counter = 0;
    while (fscanf(in, "%d",&num) != -1){
        counter++;
    }
    int *t = (int*) malloc(sizeof(*t) * counter);
    rewind(in);

    for(int i = 0; i < counter; i++){
        fscanf(in, "%d", &t[i]);
    }

    for(int i = 0; i < counter/2; i++){
        if (t[i] != t[counter-i-1]){
            cout << "NO" << endl;
            free(t);
            fclose(in);
            return 0;
        }
    }
    free(t);
    fclose(in);
    cout << "YES";
    return 0;
}
