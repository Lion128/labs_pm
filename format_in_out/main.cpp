#include <iostream>
#include <windows.h>
using namespace std;

/*
char bufRus[256];
char* Rus(const char *text)
{
	CharToOem(text, bufRus);
	return bufRus;
}

char bufEng[256];
char* Eng(const char *text)
{
	OemToChar(text, bufEng);
	return bufEng;
}
*/

class student {
	char fio[40];
	char group[5];
	char n_st[10];
public:
	student() {};
	student(char const *n, char const *g, char const *b)
	{
		strcpy(fio, n); strcpy(group, g); strcpy(n_st, b);
	}
	friend ostream& operator << (ostream &stream, student &ob);
	friend istream&  operator >> (istream  &stream, student &ob);
};

ostream &operator << (ostream &stream, student &ob)
{
	stream << ob.fio << " " << ob.group << " " << ob.n_st << endl;
	return stream;
}

istream &operator >> (istream &stream, student &ob)
{
	cout << "\n������� �������, ���, ��������: ";
	stream.getline(ob.fio, 40);
	strcpy(ob.fio, ob.fio);
	cout << "\n������� ����� ������ ��������: ";
	stream >> ob.group;
	cout << "\n������� ����� �������������� ��������: ";
	stream >> ob.n_st;
	cout << endl;

	return stream;
}

int main()
{
    setlocale(LC_ALL, "Russian");
	student ob1("������ ���� ��������", "105", "st001788");
	student ob2("������ ���� ��������", "105", "st005678");
	student ob3("������� ����� ���������", "105", "st008634");
	student ob4;

	cout << ob1 << ob2 << ob3;

	cin >> ob4;
	cout << ob4;

	system("pause");
	return 0;
}


/*
int main()
{
    char str[80] = "Something";
    ofstream mystream_out("test.txt");
    //cout << mystream;
    if (!mystream_out)
        cout << "Not ok" << endl;
    else
        cout << "Ok" << endl;
    mystream_out << str;
    mystream_out << 100 << endl << hex << 100 << endl;
    int i, j;
    char new_str[80];
    ifstream mystream_in("test.txt");
    mystream_in >> new_str;
    mystream_in >> i;
    mystream_in >> j;
    cout << new_str << endl << i << endl << j << endl;
    return 0;
}
*/
