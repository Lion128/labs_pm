#include <iostream>
#include <list>
#include <iterator>
using namespace std;

int main()
{
    list<int> mylist = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    list<int>:: iterator it;
    it = mylist.begin();
    advance(it, 1);
    copy(it, mylist.end(), ostream_iterator<int>(cout, "\n"));

    //it = mylist.end();
    //mylist.erase(mylist.begin(), it);
    /*
    for(int i = 0; i < mylist.size(); i++){
            cout << mylist.front() << endl;
    }
    */


    /*
    int dat[N] = {1, 2, 3, 4, 5, 6, 7, 8};
    sort(dat, dat + N);
    for(int i = 0; i < N; i++)
        printf("%d ", dat[i]);
    puts("");
    sort(dat, dat + N, greater<int>());
    for (int i = 0; i < N; i++)
        cout << dat[i] << " ";
    */
    return 0;
}
