#include <iostream>
#include <vector>

using namespace std;
int main(){
    int n;
    cin >> n;
    vector<int> v(n);
    for (int& c : v){
        cin >> c;
    }
    double mean = 0;
    for (int c : v)
        mean += c;
    mean /= (double)v.size();
    int counter = 0;
    for (int c : v){
        if (c > mean)
            counter++;
    }
    cout << counter << endl;
    for (int i = 0; i < v.size(); i++){
        if (v[i] > mean)
            cout << i << " ";
    }
    return 0;
}
