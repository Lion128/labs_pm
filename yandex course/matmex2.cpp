#include <iostream>
#include <cstdlib>

using namespace std;

int main(){
    int n, k;
    cin >> n >> k;
    int* arr1 = (int*) malloc (n * sizeof(int));
    int* arr2 = (int*) malloc (k * sizeof(int));
    for (int i = 0; i < n; i++)
        arr1[i] = rand();
    for (int i = 0; i < k; i++)
        arr2[i] = rand();

    for (int i = 0; i < n; i++){
        for (int j = 1; j < n; j++){
            if (arr1[j-1] > arr1[j]){
                int tmp = arr1[j-1];
                arr1[j-1] = arr1[j];
                arr1[j] = tmp;
            }
        }
    }
    int current;
    for (int j = 0; j < k; j++){
        current = arr2[j];
        for (int i = 0; i < n && current >= arr1[i]; i++){
            if (current == arr1[i])
                cout << "Found: " << current << endl;
        }
    }
    return 0;
}
