#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
//First way to create a vector on vector
/*    vector <vector <int>> v;
    for (int i = 0; i < 5; i++){
        vector <int> vec(5);
        for (int j = 0; j < 5; j++)
            vec[j] = i * j * j;
            v.push_back(vec);
    }

// Output data from vector //
    for (int i = 0; i < 5; i++){
        for (int j = 0; j < 5; j++){
            cout << v[i][j] << " ";
        }
        cout << endl;
    }
    // Another way to create vector on vector// n - strings, m - columns
    int n = 10, m = 5;
vector< vector < int > > arr(n, vector<int>(m));
for (int i = 0; i < n; i ++){
    for (int j = 0; j < m; j ++)
        cout << arr[i][j] << " ";
        cout << endl;
    }
    */
// 3- shape vector
//vector <vector <vector <int> > >(h, vector <vector <int> >(n));
/*
vector <vector <vector <int>>> v;
    for (int h = 0; h < 5; h++){
        vector <vector <int>> vec;
        for (int i = 0; i < 5; i++){
            vector <int> vec_end(5);
            for(int j = 0; j < 5; j++)
                vec_end[j] = h * i * j * j;
            vec.push_back(vec_end);
        }
        v.push_back(vec);
    }
    */
int h = 5, n = 5, m = 5;
vector <vector <vector <int>>>v(h, vector<vector<int>>(m, vector<int>(n)));

for (int h = 0; h < 5; h++){
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++){
                v[h][i][j] = i * j *h;
            }
        }
    }

/*
    for (int h = 0; h < 5; h++){
        cout << "Shape: " << h << endl;
        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++){
                printf("%4d ", v[h][i][j]);
            }
            cout << endl;
        }
        cout << endl << endl;
    }
*/
for (auto x : v){
    for (auto y : x){
        for (auto z : y){
            cout << z << " ";
        }
        cout << endl;
    }
    cout << endl << endl;
}
    return 0;
}
