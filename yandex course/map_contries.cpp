#include <iostream>
#include <map>
#include <string>
using namespace std;

bool CountryFinder(const string counrty, const map<string, string>& countries){
    return countries.count(counrty);;
}
string CapitalFinder(const string counrty, map<string, string> countries){
    return countries[counrty];
}
void PrintCountries(map<string, string> countries){
    for(const auto& [key, value] : countries)
        cout << key << '/' << value << " ";
    cout << endl;
}
int main(){
    int q;
    cin >> q;
    map <string, string> countries;
    string request, a, b;
    while (q > 0){
    cin >> request;
    if (request == "CHANGE_CAPITAL"){
        cin >> a >> b;
        if (CountryFinder(a, countries) == 0){
            cout << "Introduce new country " << a << " with capital " << b << endl;
            countries[a] = b;
        }
        else{
            if (b == CapitalFinder(a, countries)){
                cout << "Country " << a << " hasn't changed its capital" << endl;
            }
            else {
                cout << "Country " << a << " has changed its capital from " << countries[a] << " to " << b << endl;
                countries[a] = b;
            }
        }
    }
    if (request == "RENAME"){
        cin >> a >> b;
        if (a == b || !CountryFinder(a, countries) || CountryFinder(b, countries)){
            cout << "Incorrect rename, skip" << endl;
        }
        else{
            cout << "Country " << a << " with capital " << countries[a] << " has been renamed to " << b << endl;
            countries[b] = countries[a];
            countries.erase(a);
        }
    }
    if (request == "ABOUT"){
        cin >> a;
        if (CountryFinder(a, countries))
            cout << "Country " << a << " has capital " << CapitalFinder(a, countries) << endl;
        else
            cout << "Country " << a << " doesn't exist" << endl;
    }
    if (request == "DUMP"){
        if (!countries.size())
            cout << "There are no countries in the world" << endl;
        else
            PrintCountries(countries);
    }
    --q;
    }
    return 0;
}
