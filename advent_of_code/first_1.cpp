#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

int main(){
    vector<int> numbers;
    ifstream in("input_first.txt");
    while(in){
        int n;
        in >> n;
        numbers.push_back(n);
    }
    numbers.pop_back();

    for (int i = 0; i < numbers.size(); i++)
        for (int j = 0; j < numbers.size(); j++)
            for (int k = 0; k < numbers.size(); k++)
                if (i != j && j != k && i != k && numbers[i] + numbers[j] + numbers[k] == 2020)
                cout << numbers[i] * numbers[j] * numbers[k] << endl;

    /*for (const auto& n : numbers)
        cout << n << endl;
        */
    return 0;
}
