#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <ctype.h>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <exception>


using namespace std;

class Rational {
public:
  Rational(){
    p = 0;
    q = 1;
  }
  Rational(int numerator, int denominator){
      if (denominator == 0){
        throw invalid_argument("Invalid argument");
      }
      if (numerator < 0 && denominator < 0){
        p = -numerator;
        q = - denominator;
      } else if (numerator == 0){
          p = 0;
          q = 1;
      } else if (denominator < 0){
          p = -numerator;
          q = -denominator;
      } else {
          p = numerator;
          q = denominator;
      }
      if (p > 0 && q > 0){
          int devis = gcd(p, q);
          p = p / devis;
          q = q / devis;
      } else {
          //cout << "p = " << p << ", q = " << q << endl;
          int devis = gcd(-p, q);
          p = p / devis;
          q = q / devis;
      }
  }

  int Numerator() const{
      return p;
  }
  int Denominator() const{
      return q;
  }

private:
    int gcd(int first, int second){
        if (first == 0 || second == 0)
            return 1;
        if (first < second){
            int tmp = second;
            second = first;
            first = tmp;
        }
        while (1){
            first = first % second;
            int tmp = first;
            first = second;
            second = tmp;
            if (second == 0){
                return first;
            }
        }
    }
    int p = 0, q = 0;
};

bool operator==(const Rational& first, const Rational& second) {
      if (first.Numerator() == second.Numerator() && first.Denominator() == second.Denominator())
            return true;
      return false;
  }
Rational operator+(const Rational& first, const Rational& second){
    int tmp1 = first.Numerator() * second.Denominator();
    int tmp2 = second.Numerator() * first.Denominator();
    int denom = second.Denominator() * first.Denominator();
    return {tmp1+tmp2, denom};
}
Rational operator-(const Rational& first, const Rational& second){
    int tmp1 = first.Numerator() * second.Denominator();
    int tmp2 = second.Numerator() * first.Denominator();
    int denom = second.Denominator() * first.Denominator();
    return {tmp1-tmp2, denom};
}
Rational operator*(const Rational& first, const Rational& second){
    return {first.Numerator() * second.Numerator(), first.Denominator()*second.Denominator()};
}
Rational operator/ (const Rational& first, const Rational& second){
    if (second.Numerator() == 0)
        throw domain_error("Division by zero");
    return {first.Numerator() * second.Denominator(), first.Denominator() * second.Numerator()};
}

bool operator<(const Rational& lhs, const Rational& rhs) {
    int lhs_num = lhs.Numerator() * rhs.Denominator();
    int rhs_num = rhs.Numerator() * lhs.Denominator();
    return lhs_num < rhs_num;
}

ostream& operator<< (ostream& stream, const Rational& rat){
    stream << rat.Numerator();
    stream << "/";
    stream << rat.Denominator();
    return stream;
}
istream& operator>> (istream& stream, Rational& rational){
    int num = -999, denum = -999;
    char separ;
    stream >> num;
    if (!stream.operator bool())
        return stream;
    stream >> separ;
    if (!stream.operator bool())
        return stream;
    stream >> denum;
    if (!stream.operator bool())
        return stream;
    if (separ == '/')
        rational = {num, denum};
    return stream;
}


int main() {
    Rational r1, r2;
    char operation;
    try {
        cin >> r1;
        cin >> operation;
        cin >> r2;
    } catch (invalid_argument& e) {
        cout << e.what() << endl;
        return 1;
    }
    Rational res = {0, 1};
    if (operation == '+'){
        res = r1 + r2;
    } else if (operation == '-'){
        res = r1 - r2;
    } else if (operation == '*'){
        res = r1 * r2;
    } else if (operation == '/'){
        try {
            res = r1 / r2;
        } catch (domain_error& e) {
            cout << e.what() << endl;
            return 2;
        }
    }
    cout << res << endl;
    return 0;
}

