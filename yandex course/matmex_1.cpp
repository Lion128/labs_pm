#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main(){
    ifstream input("input_mat_1.txt");
    int c;
    int* arr = (int*) malloc(0 * sizeof(int));
    int i = 0;
    while(input.operator bool()){
        realloc(arr,(i+1)*sizeof(int));
        input >> c;
        arr[i] = c;
        i++;
        input.ignore(1);
    }
    for(int j = 0; j < i/2; j++)
        if (arr[j] != arr[i-j-1]){
            cout << "No" << endl;
            return 0;
        }
    cout << "Yes" << endl;
    return 0;
}
