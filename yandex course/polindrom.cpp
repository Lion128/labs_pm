#include <iostream>
#include <string>

using namespace std;

bool IsPalindrom(string x){
    int n = x.length();
    for (int i = 0; i < n; ++i){
        if (x[i] != x[n - 1 - i])
            return false;
    }
    return true;

}

int main(){
    string a;
    cin >> a;
    if (IsPalindrom(a))
        cout << "true" << endl;
    else
        cout << "false" << endl;
    return 0;
}
