#include <iostream>
#include <string>
#include <map>
#include <set>

using namespace std;

int main(){
    int number_of_operations;
    cin >> number_of_operations;
    string s;
    map<string, set<string>> total;
    for (int i = 0; i < number_of_operations; i++){
        cin >> s;
        if (s == "ADD"){
            string first_word, second_word;
            cin >> first_word >> second_word;
            total[first_word].insert(second_word);
            total[second_word].insert(first_word);

        }
        if (s == "COUNT"){
            string word;
            cin >> word;
            cout << total[word].size() << endl;
        }
        if (s == "CHECK"){
            string first_word, second_word;
            cin >> first_word >> second_word;
            if (total[first_word].count(second_word))
                cout << "YES" << endl;
            else
                cout << "NO" << endl;
        }
    }
    return 0;
}
