#include <iostream>

using namespace std;

typedef struct Node{
    int data;
    Node* left;
    Node* right;
} Node;

void tree_print(Node* tree){
    if(tree == NULL)
        return;
    tree_print(tree -> left);
    cout << tree -> data << " ";
    tree_print(tree->right);
}

Node* tree_add(Node* tree, int number){
    if(tree == NULL){
        Node *t =(Node*) malloc(sizeof(Node));
        t -> data = number;
        t -> left = t -> right = NULL;
        return t;
    }
    if(number > tree -> data){
        tree -> right = tree_add(tree -> right, number);
    }
    if(number < tree -> data){
        tree -> left = tree_add(tree -> left, number);
    }
        return tree;
}
Node* tree_search(Node* tree, int number){
    if(tree == NULL){
        cout << "Not found: " << number << endl;
        return NULL;
    }
    if(tree -> data == number){
        cout << "Found: " << number << endl;
        return tree;
    }
    if(number > tree -> data)
        tree -> right = tree_search(tree -> right, number);
    if(number < tree -> data)
        tree -> left = tree_search(tree -> left, number);
    return tree;
}
int main()
{
    Node *tree = NULL;
    //Node one = {1, NULL, NULL};
    //tree = &one;
    //Node second = {2, NULL, NULL};
    //one.right = &second;
    //tree_print(tree);
    //tree = tree_add(tree, 10);
    //cout << endl << "WE have after addition" << endl;
    //tree_print(tree);

    // �� ������� ����� ������� 2 ������� � ��������� �� ���������� �������
    int n, k;
    cout << "Inter n: ";
    cin >> n;
    cout << "Enter k: ";
    cin >> k;
    int* mas_first = (int*) malloc(sizeof(int*) * n);
    int* mas_second = (int*) malloc(sizeof(int*) * k);
    for(int i = 0; i < n; i++)
        *(mas_first + i) = rand();
    for(int i = 0; i < k; i++)
        *(mas_second + i) = rand();

    /*// ��������
    for(int i = 0; i < n; i++)
        cout << *(mas_first + i) << " ";
    cout << endl;
    for(int i = 0; i < k; i++)
        cout << *(mas_second + i) << " ";
*/
    // ����� ���������� ������� ������ ������ ��������� ������� 2 � ������� 1
    // ���������� �������� � ������
    for(int i = 0; i < n; i++){
        tree = tree_add(tree, *(mas_first + i));
    }
    for(int i = 0; i < k; i++){
        tree = tree_search(tree, *(mas_second + i));
    }
    free(mas_first);
    free(mas_second);


    /*// Check
    for(int i = 0; i < n; i++)
        cout << *(mas_first + i) << " ";
    cout << endl;
    for(int i = 0; i < k; i++)
        cout << *(mas_second + i) << " ";
    */
    //cout << tree->right << endl;
    return 0;
}
