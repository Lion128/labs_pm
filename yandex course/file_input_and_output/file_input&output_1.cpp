#include <iostream>
#include <fstream>

using namespace std;

int main(){
    string path = "input.txt";
    ifstream input(path);
    if (input){
        string line;
        while(getline(input, line)){
            cout << line << endl;
        }
    } else {
        cout << "error!" << endl;
    }
    return 0;
}
