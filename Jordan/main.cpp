#include <iostream>
#include <vector>
using namespace std;


int drow(int a, int n, int offset, FILE* out){
    int** mas = (int**) calloc(sizeof(int), n);
    for (int i = 0; i < n; i++)
        mas[i] = (int*) calloc(sizeof(int), n);

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (i == j)
                mas[i][j] = a;
        }
    }

    for (int i = 0; i < n - 1; i++){
        for (int j = 0; j < n; j++){
            if (i + 1 == j)
                mas[i][j] = 1;
        }
    }


    for (int i = 0; i < n; i++){
        for (int k = 0; k < 2 * offset; k++)
            fprintf(out, " ");
            for (int j = 0; j < n; j++){
                    fprintf(out, "%d ", mas[i][j]);
                //cout << mas[i][j] << " ";
            }
            fprintf(out, "\n");
    }
    offset += n;
    return offset;
}

int for_one_nuber(int a, int offset, FILE* out)
{
    int dim;
    cout << "������� ������������ ����������� ��� ������. ����� " << a << ": ";
    cin >> dim;
    vector <int> v(dim);
    vector <int> delta(dim);
    cout << "������� ����������� ���� ��� ���������, ������� � �������" << endl;
    for (int i = 0; i < dim; i++)
        cin >> v[i];

    delta[0] = v[0];
    for(int i = 0; i < dim - 1; i++){
        delta[i+1] = v[i+1] - v[i];
    }
    //cout << endl << "����������� ������ �������:" << endl;
    for(int i = dim - 1; i >= 0; i--){
        while(delta[i] > 0){
            //cout << "������ ��������: " << i + 1 << endl;
            offset = drow(a, i+1, offset, out);
            for(int j = 0; j <= i; j++){
                delta[j]--;
            }
        }
    }
    return offset;
}
int main()
{
    setlocale(LC_ALL, "");
    int offset = 0;
    int num;
    cout << "������� ���������� ����������� �����: ";
    cin >> num;
    vector <int> v_own(num);
    for (int i = 0; i < num; i++){
        cout << "������� " << i + 1 << "-�� ����������� �����: ";
        cin >> v_own[i];
    }
    FILE *out;
    out = fopen ("output.txt", "w+");
    for (auto a : v_own)
        offset = for_one_nuber(a, offset, out);
    fclose(out);
    return 0;
}
