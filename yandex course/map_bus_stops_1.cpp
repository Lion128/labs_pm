#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

void PrintMap(const map<string, vector<string>>& x){
    if (x.size()){
        for (const auto& [key, value] : x){
            cout << "Bus " << key << ": ";
            //fprintf(fp, "Bus %s: ", key);
            for (const auto& c : value){
                cout << c << " ";
                //fprintf(fp, "%s", c);
            }
            cout << endl;
            //fprintf(fp, "\n");
        }
    }
    else{
        cout << "No buses" << endl;
        //fprintf(fp, "No buses");
    }
}
bool is_has_stop(const string& stop, const vector<string>& stops){
    for (const auto& c : stops)
        if(c == stop)
            return true;
    return false;
}
int main(){
    //FILE* fp;
    //fp = fopen("result.txt", "a+");
    string operation_code;
    vector<string> stop_order;
    map<string, vector<string>> buses;
    int q;
    cin >> q;
    while (q > 0){
        cin >> operation_code;
        if (operation_code == "NEW_BUS"){
            string bus_name;
            cin >> bus_name;
            stop_order.push_back(bus_name);
            buses[bus_name];
            int number_of_stops;
            cin >> number_of_stops;
            string stop_name;
            for (int i = 0; i < number_of_stops; ++i){
                cin >> stop_name;
                buses[bus_name].push_back(stop_name);
            }

        }
        if (operation_code == "BUSES_FOR_STOP"){
            string stop;
            cin >> stop;
            int counter = 0;
            for (const auto& c : stop_order)
                if (is_has_stop(stop, buses[c])){
                    cout << c << " ";
                    //fprintf(fp, "%s ", key);
                    ++counter;
                }
                if (!counter)
                    cout << "No stop";
                    //fprintf(fp, "%s", "No stop");
            cout << endl;
            //fprintf(fp, "\n");
        }
        if (operation_code == "STOPS_FOR_BUS"){
            string bus_name;
            cin >> bus_name;
            if (buses.count(bus_name)){
                int flag = 0;
                const vector<string> stops = buses[bus_name];
                for (const auto& c: stops){
                    cout << "Stop " << c << ": ";
                    //fprintf(fp, "Stop %s:", c);
                    for (const auto& t : stop_order)
                        if (is_has_stop(c, buses[t]) && t != bus_name){
                            cout << t << " ";
                            //fprintf(fp, "%s", key);
                            flag = 1;
                        }
                if (flag == 0)
                    cout << "no interchange";
                    //fprintf(fp, "no interchange");
                cout << endl;
                //fprintf(fp, "\n");
                flag = 0;
                }
            }
            else{
                cout << "No bus" << endl;
                //fprintf(fp, "No bus");
            }
        }
        if (operation_code == "ALL_BUSES"){
            PrintMap(buses);
        }
        --q;
    }
    return 0;
}
