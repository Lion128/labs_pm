#include <iostream>

using namespace std;

int main(){
    unsigned int n = 0, r = 0;
    cin >> n >> r;
    uint64_t sum = 0;
    while (n > 0){
        uint64_t w = 0, h = 0, d = 0;
        cin >> w >> h >> d;
        uint64_t size_of_block = w * h * d;
        sum += size_of_block;
        n--;
    }
    cout << sum * r << endl;
    return 0;
}
