set<string> BulidMapValuesSet(const map<int, string>& m){
    set<string> res;
    for (auto [key, value] : m){
        res.insert(value);
    }
    return res;
}
