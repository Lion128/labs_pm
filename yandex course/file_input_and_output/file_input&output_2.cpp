#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

int main(){
    string inp_path = "input.txt";
    ifstream input(inp_path);
    vector<string> lines;
    if (input){
        string line;
        while(getline(input, line)){
            lines.push_back(line);
        }
    } else {
        cout << "error!" << endl;
    }
    string out_path = "output.txt";
    ofstream output(out_path);
    for (const string& str : lines){
        output << str << endl;
    }
    return 0;
}
