#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<int> v(n);
    for (auto& i : v){
        cin >> i;
    }
    sort(begin(v), end(v), [](int x, int y){
         if (abs(x) < abs(y))
            return true;
        return false;
         });
    for (const auto& i : v){
        cout << i << " ";
    }
}
