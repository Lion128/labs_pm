#include <iostream>

using namespace std;

enum class Request_type{
    ADD,
    MOVE,
    DEL
};

int main(){
    cout << static_cast<int>(Request_type::ADD) << endl;
    cout << static_cast<int>(Request_type::MOVE) << endl;
    cout << static_cast<int>(Request_type::DEL) << endl;
    return 0;
}
