#include <iostream>
#include <vector>

using namespace std;

int main(){
    int a;
    cin >> a;
    vector <int> v;
    while (a != 0){
        v.push_back(a % 2);
        a /= 2;
    }
    for (int i = v.size() - 1; i >= 0; --i){
        cout << v[i];
    }
    return 0;
}
