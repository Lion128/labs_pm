#include <iostream>
#include <string>
#include <map>
#include <set>

using namespace std;

set<string> BulidMapValuesSet(const map<int, string>& m){
    set<string> res;
    for (auto [key, value] : m){
        res.insert(value);
    }
    return res;
}

int main(){
    map<int, string> m = {{1, "odd"}, {2, "even"}, {3, "odd"}, {4, "even"}};
    set<string> values = BulidMapValuesSet(m);
    for (auto value : values)
        cout << value << endl;
    return 0;
}
