#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<bool> q;
    for(int i = 0; i < n; i++){
        int sum = 0;
    int tmp;
    int s;
    string operation;
    cin >> operation;
    if (operation == "WORRY"){
        cin >> tmp;
        q[tmp] = true;
    }
    if (operation == "QUIET"){
        cin >> tmp;
        q[tmp] = false;
    }
    if (operation == "COME"){
        cin >> tmp;
        s = q.size();
        q.resize(s + tmp);
    }
    if (operation == "WORRY_COUNT"){
        for(bool c : q)
            if(c)
                sum++;
        cout << sum << endl;
        sum = 0;
    }
    }
    return 0;
}
