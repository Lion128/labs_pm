#include <iostream>
#include <set>
#include <string>

using namespace std;

int main(){
    set<string> lt = {"a","b", "ac", "ab", "ad", "aa"};
    for (const auto& item : lt)
        cout << item << " ";
    return 0;
}
