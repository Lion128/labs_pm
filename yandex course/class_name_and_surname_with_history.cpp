#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

class Person{
public:
    void ChangeFirstName(int year, const string& first_name){
        name_history[year] = first_name;
    }
    void ChangeLastName(int year, const string& last_name){
        surname_history[year] = last_name;
    }
    string GetFullName(int year){
        string first_name = GetName(year);
        string last_name = GetSurname(year);
        if (first_name.empty() && last_name.empty()){
            return "Incognito";
        }
        else if (first_name.empty()){
            return last_name + " with unknown first name";
        }
        else if (last_name.empty()){
            return first_name + " with unknown last name";
        }
        else {
            return first_name + " " + last_name;
        }
    }
    string GetFullNameWithHistory(int year){
        string str = "Incognito";
        string str_names;
        string str_surnames;

        vector<string> names;
        string prev_name;
        for (const auto& item : name_history){
            if (item.first <= year && item.second != prev_name){
                names.push_back(item.second);
                prev_name = item.second;
            }
        }
        reverse(begin(names), end(names));

        if (names.size() > 0){
            str_names = names[0];
        }
        if (names.size() > 1){
            str_names += " (";
            for (int i = 1; i < names.size(); i++){
                str_names += names[i];
                if (i < names.size() - 1)
                    str_names += ", ";
            }
            str_names += ")";
        }



        vector<string> surnames;
        string prev_surname;
        for (const auto& item : surname_history){
            if (item.first <= year && item.second != prev_surname){
                surnames.push_back(item.second);
                prev_surname = item.second;
            }
        }
        reverse(begin(surnames), end(surnames));

        if (surnames.size() > 0){
            str_surnames = surnames[0];
        }
        if (surnames.size() > 1){
            str_surnames += " (";
            for (int i = 1; i < surnames.size(); i++){
                str_surnames += surnames[i];
                if (i < surnames.size() - 1)
                    str_surnames += ", ";
            }
            str_surnames += ")";
        }
        if (str_names.empty() && str_surnames.empty()){
            str = "Incognito";
        } else if (str_surnames.empty()){
            str = str_names + " with unknown last name";
        } else if (str_names.empty()){
            str  = str_surnames + " with unknown first name";
        }else {
            str = str_names + " " + str_surnames;
        }
        return str;
    }
private:
    map<int, string> name_history;
    map<int, string> surname_history;
    string GetName(int year){
        string name;
        int name_year = 0;
        for (const auto& item : name_history)
            if (item.first <= year)
                name_year = item.first;
        if (name_year != 0)
            name = name_history[name_year];
        return name;
    }
    string GetSurname(int year){
        int surname_year = 0;
        string surname;
        for (const auto& item : surname_history)
            if (item.first <= year)
                surname_year = item.first;
        if (surname_year != 0)
            surname = surname_history[surname_year];
        return surname;
    }
};

    int main() {
  Person person;

  person.ChangeFirstName(1965, "Polina");
  person.ChangeLastName(1967, "Sergeeva");
  for (int year : {1900, 1965, 1990}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1970, "Appolinaria");
  for (int year : {1969, 1970}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1968, "Volkova");
  for (int year : {1969, 1970}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeFirstName(1990, "Polina");
  person.ChangeLastName(1990, "Volkova-Sergeeva");
  cout << person.GetFullNameWithHistory(1990) << endl;

  person.ChangeFirstName(1966, "Pauline");
  cout << person.GetFullNameWithHistory(1966) << endl;

  person.ChangeLastName(1960, "Sergeeva");
  for (int year : {1960, 1967}) {
    cout << person.GetFullNameWithHistory(year) << endl;
  }

  person.ChangeLastName(1961, "Ivanova");
  cout << person.GetFullNameWithHistory(1967) << endl;

  return 0;
}
