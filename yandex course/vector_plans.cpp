#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int main(){
    int n;
    cin >> n;
    setlocale(LC_ALL, "Russian");
    string command;
    string doing_name;
    vector<int> months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int day;
    int flag = 0;
    vector<string> doings(31, " ");

    while (n > 0){
        cin >> command;
        if (command == "ADD"){
            cin >> day;
            cin >> doing_name;
            doings[day-1] += doing_name;
            doings[day-1] += " ";
        }

        if (command == "DUMP"){
            cin >> day;
            cout << count(doings[day-1].begin(), doings[day-1].end(), ' ') << " " << doings[day-1] << endl;
        }

        if (command == "NEXT"){
            flag++;
            flag %= 12;
            if (months[flag] < months[flag - 1]){
                for (int  i = months[flag - 1] - 1; i > months[flag] - 1; --i){
                    doings[months[flag] - 1] += doings[i];
                }
            }
            doings.resize(months[flag]);
        }
        if (command == "PRINT"){
            FILE* out;
            out = fopen("plans.txt", "a+");
            for (string s : doings)
                fprintf(out, "%s", s);
        }
        n--;
    }
    /*for (int i = 0; i < doings.size(); ++i)
        cout << i + 1 << ": " << doings[i] << endl;
    cout << endl;
    */
    return 0;
}
