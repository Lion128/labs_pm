#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Date{
    int day;
    int month;
    int year;
};

struct Student{
    Student(const string& name, const string& surname, const Date& bith){
        first_name = name;
        last_name = surname;
        date = bith;
    }
    string first_name;
    string last_name;
    Date date;
};
int main(){
    vector<Student> students;
    int number_of_students;
    cin >> number_of_students;
    string first;
    string second;
    int day, month, year;
    for (int i = 0; i < number_of_students; i++){
        cin >> first >> second;
        cin >> day >> month >> year;
        students.push_back({first, second, {day, month, year}});
    }
    int m;
    cin >> m;
    string operation;
    int number;
    for (int i = 0; i < m; i++){
        cin >> operation;
        cin >> number;
        number--;
        if (operation == "name" && number >= 0 && number <= students.size()){
            cout << students[number].first_name << " " << students[number].last_name;
        } else if (operation == "date" && number >= 0 && number <= students.size()){
            cout << students[number].date.day << "." << students[number].date.month << "." << students[number].date.year;
        } else {
            cout << "bad request";
        }
        cout << endl;
    }
    return 0;
}
